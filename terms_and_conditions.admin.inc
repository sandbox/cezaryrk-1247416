<?php


function terms_and_conditions_admin_overview($form, $form_state){
  $list = TAC::getAll();  
  $selection = TAC::getActive();  
  $statistics = TACUser::getStatistic();
  $roles_names = user_roles(TRUE);
  
  $form['#tree'] = true;
  $form[TACVoc::TYPE] = array();
  
  $counter = 0;
  foreach($list as $tcid => $obj){
    
    $list = TACVersion::getAllRevisions($tcid);
    $options = array();
    
    $entry = array();
    $entry['vid'] = array();

    $entry["tcid"] = array(
        	'#type' => 'checkbox',
        	'#default_value'=> ($obj->active_vid ? true : false),
        	"#return_value"=>$tcid,
        	'#title' => $obj->label,
    );
    
    $ops = array();
    $ops[] = l(t("New revision"),TACVoc::ADMIN_PATH."/$tcid/add");
    $ops[] = l(t("Delete"),TACVoc::ADMIN_PATH."/$tcid/delete");
    
    $entry["ops"] = array(
            '#markup' => implode(", ",$ops),
    );
    
    $entry['weight'] = array(
              '#type' => 'weight',
              '#title' => t('Weight for @title', array('@title' => $tcid)),
              '#title_display' => 'invisible',
              '#default_value' => $obj->weight,
    );
    
    
    foreach($list as $vid => $rev){
      $t_version = TACVersion::load($tcid, $vid);
      
      $uri = TACVoc::ADMIN_PATH."/".$tcid."/revision/".$vid."/view";
      
      $row_data = array();
      $row_data['title'] = l(TACUtil::getTitle($t_version, t("No title set for terms")),$uri);
      $row_data['required'] = TaCUtil::getRequired($t_version,t("No required is set"));
      $row_data['mode'] = TaCUtil::getEnabledModes($t_version);
      $row_data['mode'] = !empty($row_data['mode']) ? implode(", ", $row_data['mode']) : t("No mode specified");
      $row_data['role_enable'] = TaCUtil::getEnableRole($t_version);
      $row_data['role_enable'] = $row_data['role_enable'] ? $roles_names[$row_data['role_enable']] : t("No role is set"); 
      $row_data['role_act'] = TaCUtil::getActOnRole($t_version);
      $row_data['role_act'] = $row_data['role_act'] ? $roles_names[$row_data['role_act']] : t("Act on all roles");
      $row_data['timestamp'] = date("d.m.Y H:i:s",$t_version->timestamp);
      $row_data['users'] = isset($statistics[$tcid][$vid]) ? $statistics[$tcid][$vid] : 0;
      
      $entry['vid'][$vid]['rvid'] = array(
      	'#type'=>'radio',
      	'#value' => ($obj->active_vid == $vid ? true : false),      	
      	'#name'=>TACVoc::TYPE."[$tcid][rvid]",'#return_value'=>$vid);
      foreach($row_data as $_row_key => $_markup){
        $entry['vid'][$vid][$_row_key] = array('#markup' => $_markup);
      }
      
      
      $ops = array();
      
      
      if($row_data['users'] > 0){
        if($obj->active_vid != $vid){
          $ops[] = l(t("Clone"), TACVoc::ADMIN_PATH."/".$tcid."/revision/".$vid."/clone");
          $ops[] = l(t("Detach users"), TACVoc::ADMIN_PATH."/".$tcid."/revision/".$vid."/clear");
        }else{
          $ops[] = l(t("Clone"), TACVoc::ADMIN_PATH."/".$tcid."/revision/".$vid."/clone");
        }
      }else{
        if($obj->active_vid != $vid){  
          $ops[] = l(t("Edit"), TACVoc::ADMIN_PATH."/".$tcid."/revision/".$vid."/edit");
          $ops[] = l(t("Clone"), TACVoc::ADMIN_PATH."/".$tcid."/revision/".$vid."/clone");
          $ops[] = l(t("Delete"), TACVoc::ADMIN_PATH."/".$tcid."/revision/".$vid."/delete");
        }else{
          $ops[] = l(t("Clone"), TACVoc::ADMIN_PATH."/".$tcid."/revision/".$vid."/clone");          
        }
      }      
            
      $entry['vid'][$vid]['ops'] = 
        array('#markup' => implode(", ",$ops));

    }
    
    $form[TACVoc::TYPE][$tcid] = $entry;
  }
  
  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Save changes'));
  return $form;
}


function terms_and_conditions_admin_overview_submit($form, &$form_state){
  drupal_set_message("Terms saved");
  
  $tac = $form_state['input'][TACVoc::TYPE];
  
  foreach($tac as $tcid => $data){
    if(!empty($data['tcid']) && !empty($data['rvid'])){
      db_update(TACVoc::TABLE)->fields(array("active_vid"=>$data['rvid'],"weight"=>$data['weight']))->condition(TACVoc::ID,$tcid)->execute();      
    }else{
      db_update(TACVoc::TABLE)->fields(array("active_vid"=>0,"weight"=>$data['weight']))->condition(TACVoc::ID,$tcid)->execute();      
    }
  }
  drupal_goto(TACVoc::ADMIN_PATH);
}

/**
* Returns HTML for the text format administration overview form.
*
* @param $variables
*   An associative array containing:
*   - form: A render element representing the form.
*
* @ingroup themeable
*/
function theme_terms_and_conditions_admin_overview($variables) {
  $form = $variables['form'];
  
  $header = array();
  $header_rev = array(
    t(''),
    t('Title'), 
    t('Verification'), 
    t('Mode'), 
    t('Act on role'),
    t('Enable role'), 
    t('Changed'),
    t('Users'),
    t('Operations'));
  $tac_id = TACVoc::TYPE;
  
  $rows = array();
  foreach (element_children($form[$tac_id]) as $id) {
    $form[$tac_id][$id]['weight']['#attributes']['class'] = array('text-format-order-weight');
    
    $rev_rows = array();
    foreach (element_children($form[$tac_id][$id]['vid']) as $rid) {
      $rev_rows[] = array('data'=>array(
        drupal_render($form[$tac_id][$id]['vid'][$rid]['rvid']),
        drupal_render($form[$tac_id][$id]['vid'][$rid]['title']),
        drupal_render($form[$tac_id][$id]['vid'][$rid]['required']),
        drupal_render($form[$tac_id][$id]['vid'][$rid]['mode']),
        drupal_render($form[$tac_id][$id]['vid'][$rid]['role_act']),
        drupal_render($form[$tac_id][$id]['vid'][$rid]['role_enable']),
        drupal_render($form[$tac_id][$id]['vid'][$rid]['timestamp']),
        drupal_render($form[$tac_id][$id]['vid'][$rid]['users']),
        drupal_render($form[$tac_id][$id]['vid'][$rid]['ops']),
      ));
    } 
    
    $revision_table = theme('table', array('rows' => $rev_rows,'header'=>$header_rev));
    $rows[] = array(
      'data' => array(
        drupal_render($form[$tac_id][$id]['tcid']).
        drupal_render($form[$tac_id][$id]['ops']).
        $revision_table,    
        drupal_render($form[$tac_id][$id]['weight']),
    ),
      'class' => array('draggable'),
    );
  }
  
  $output = theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'text-format-order')));
  $output .= drupal_render_children($form);

  drupal_add_tabledrag('text-format-order', 'order', 'sibling', 'text-format-order-weight');
  return $output;
}




