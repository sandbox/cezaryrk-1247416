<?php

class TACUtil {
  
  static function checkRole($rid){
    $user_roles = user_roles(TRUE);
    if(is_numeric($rid) && $rid > 1 && isset($user_roles[$rid])){
      return $user_roles[$rid];
    }
    return false;
  }
  

  
  static function hash(){
    $hash = func_get_args();
    if(empty($hash)) return;
    $hash = sha1(implode(":",$hash));
    return $hash;
  }
  
  static function map($key_name, $array){
    $tmp = array();
    foreach($array as $value){
      $key = $value[$key_name];
      $tmp[$key] = $value;
    }
    return $tmp;
  }
  
  
  static function getFieldItemValue($entity,$field_name,$fallback = null){
    $items = field_get_items(TACVoc::ENTITY_TYPE, $entity, $field_name);
    if(isset($items[0]['value'])) return $items[0]['value'];
    return $fallback;
  }
  
  static function getFieldItemsValues($entity,$field_name,$fallback = array()){
    $items = field_get_items(TACVoc::ENTITY_TYPE, $entity, $field_name);
    $values = array();
    if($items){
      foreach($items as $v){
        $values[] = $v['value'];
      }
      return $values;
    }else{
      return $fallback;
    }
    
  }
  
  
  static function getTitle(TACVersion $t_version, $fallback = null){
    return self::getFieldItemValue($t_version, TACVoc::F_TITLE, $fallback);
  }
  
  static function getContent(TACVersion $t_version, $fallback = null){
    return self::getFieldItemValue($t_version, TACVoc::F_BODY, $fallback);
  }

  static function getCheckboxes(TACVersion $t_version, $fallback = array()){
    return self::getFieldItemsValues($t_version, TACVoc::F_CHECKBOXES, $fallback);
  }
  
  static function getEnableRole(TACVersion $t_version, $fallback = null){
    return self::getFieldItemValue($t_version, TACVoc::F_ENABLE_ROLE, $fallback);
  }
  
  static function getActOnRole(TACVersion $t_version, $fallback = null){
    return self::getFieldItemValue($t_version, TACVoc::F_ON_ROLE, $fallback);
  }
  
  static function getRequired(TACVersion $t_version, $fallback = null){
    return self::getFieldItemValue($t_version, TACVoc::F_REQUIRED, $fallback);
  }
  
  static function getEnabledModes(TACVersion $t_version, $fallback = array()){
    return self::getFieldItemsValues($t_version, TACVoc::F_MODE, $fallback);
  }
  
  static function extractIds($t_obj){
    $tcid = null;
    $vid = null;
    
    if(is_object($t_obj)){
      $tcid = $t_obj->{TACVoc::ID};
      $vid = $t_obj->{TACVoc::VID};      
    }else if(is_array($t_obj)){
      $tcid = $t_obj[TACVoc::ID];
      $vid = $t_obj[TACVoc::VID];      
    }else{
      // @todo throw exception
    }
    
    return array($tcid,$vid);
  }
  
  /**
  * Returns the selected checkboxes and
  */
  static function filterFormCheckboxes($values, $map){
    $checked = array();
    foreach($values as $hash => $data){
      if(isset($map[$hash])){
        if(!empty($data) && is_array($data)) {
          $entry = array_shift($data);
          if($entry == 0) continue;
          $checked[$hash] = $map[$hash];
        }
      }
    }
    return $checked;
  }
  
  static function addArgumentsFrom(array $args, $index, array &$add_array ){    
    if(count($args) > $index){      
      for($i=$index;$i<count($args);$i++){
        $add_array[] = $args[$i];
      }
    }    
  }
  
}