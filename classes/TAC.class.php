<?php

class TAC extends TACObject {

  const BUTTON_DELETE = "Delete terms and conditions";

  const FORM_STATE_T_ENTRY = "t_entry";

  public $vid = 0;
  public $label = "";
  public $created = 0;
  public $changed = 0;
  public $active_vid = 0;
  public $weight = 0;


  public function __construct($tcid = null){
    if(is_null($tcid)){
      $this->is_new = true;
    }else{
      $this->is_new = false;
      $this->tcid = $tcid;
      $entry = db_select(TACVoc::TABLE)->fields(TACVoc::TABLE)->condition(TACVoc::ID,$tcid)->execute()->fetch();
      foreach($entry as $k=>$v) $this->{$k} = $v;
    }
  }


  public static function load($tcid, $reset = FALSE){
    $entity = entity_load(TACVoc::ENTITY_TYPE, array($tcid),array(), $reset);
    $entity = $entity ? reset($entity) : FALSE;

    $tac = new TAC($tcid);
    if($entity){
      foreach($entity as $key => $value){
        $tac->{$key} = $value;
      }
    }
    return $tac;
  }


  public static function form($form, &$form_state, TAC $entity = null){
    if(!$entity){
      $entity = new TAC();
    }
    $form_state[TACVoc::TYPE] = $entity;

    $form['label'] = array(
      "#type" => "textfield",
      "#title" => t("Name"),
      '#required' => true,
      '#default_value' => $entity->label,
      '#description' => t("Internal name for the terms and conditions")
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => t('Save'),
            '#access' => true,

            '#weight' => 19,
    );

    return $form;
  }

  public static function form_submit($form, &$form_state){
    $entity = $form_state[TACVoc::TYPE];
    $entity->label = $form_state['values']['label'];
    entity_form_submit_build_entity(TACVoc::ENTITY_TYPE,$entity, $form, $form_state);
    $entity->save();
    $form_state['redirect'] = TACVoc::ADMIN_PATH;
  }

  public static function form_delete_confirm($form, &$form_state, $tcid = null){
    $t_entry = null;
    if(is_numeric($tcid)){
      $t_entry = self::load($tcid, TRUE);
    }

    if(is_null($t_entry)){
      // TODO throw exception and use centralized message handler or better watchdog
      drupal_set_message("Can't delete the terms and conditions. Wrong parameter.","error");
      drupal_goto(TACVoc::ADMIN_PATH);
    }

    if($t_entry->active_vid > 0){
      drupal_set_message("Can't delete the terms and conditions, because there are active.","error");
      drupal_goto(TACVoc::ADMIN_PATH);
    }



    $form_state[self::FORM_STATE_T_ENTRY] = $t_entry;

    $form[TACVoc::TYPE] = array(
        '#markup' => t("Delete this term and conditions with revision permanently (all user will be detached)?")
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['delete'] = array(
        '#type' => 'submit',
        '#value' => t(self::BUTTON_DELETE),
        '#access' => true,
        '#weight' => 19,
    );

    $form['actions']['no_delete'] = array(
        '#type' => 'submit',
        '#value' => t('Back'),
        '#access' => true,
        '#weight' => 20,
    );
    return $form;
  }

  public static function form_delete_confirm_submit($form, &$form_state){
    if($form_state['values']['op'] == t(self::BUTTON_DELETE)){
      $form_state[self::FORM_STATE_T_ENTRY]->delete();
      drupal_set_message("Terms and conditions permanently deleted.");
    }
    drupal_goto(TACVoc::ADMIN_PATH);
  }



  public function insert(){
    $this->created = $this->changed = REQUEST_TIME;
    drupal_write_record(TACVoc::TABLE, $this);
  }

  public function update(){
    drupal_write_record(TACVoc::TABLE, $this,TACVoc::ID);
  }


  public function delete(){
    $tcid = $this->{TACVoc::ID};
    $revisions = TACVersion::getAllRevisions($tcid);
    // detach users and delete revisions
    foreach($revisions as $vid => $data){
      $obj = TACVersion::load($tcid, $vid);
      $obj->detachUsers();
      $obj->delete(TRUE);
    }
    db_delete(TACVoc::TABLE)->condition(TACVoc::ID,$tcid)->execute();
  }

  public static function getAll() {
    return db_select(TACVoc::TABLE)
      ->fields(TACVoc::TABLE)
      ->orderBy("weight")->execute()->fetchAllAssoc(TACVoc::ID);
  }

  /**
   * Get the current active terms_and_conditions as associative array hash => tac_db_entry
   *
   * Enter description here ...
   * @param unknown_type $reset
   */
  public static function getActive($reset = FALSE) {
    static $active;
    if(!isset($active) || $reset){
      $tmp = db_select(TACVoc::TABLE)
        ->fields(TACVoc::TABLE)
        ->orderBy("weight")
        ->condition("active_vid",0,">")->execute()->fetchAllAssoc(TACVoc::ID);
      $active = array();
      foreach($tmp as $obj){
        $hash = TaCUtil::hash($obj->tcid,$obj->active_vid);
        $active[$hash] = $obj;
      }
    }
    return $active;
  }

}