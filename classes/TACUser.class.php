<?php


class TACUser {

  const MSG_USER_ACCEPT_TERM = "User accept terms";
  const MSG_USER_REVOKE_TERM = "User revoke terms";
  const MSG_USER_ACCEPT_TERM_CHANGE = "User accept new revision of terms";

  const MSG_ROLE_ADD = "Add role %s";
  const MSG_ROLE_REMOVE = "Remove role %s";

  const ROLE_REMOVE = 0;
  const ROLE_ADD = 1;


  private $uid;
  private $tids = array();
  private $roles = array();

  /**
   * @return TACUser
   */
  public static function &getInstance($uid){
    static $user_terms_and_conditions;
    if(!is_numeric($uid) || !($uid > 0)){
      // @todo throw an exception
    }
    if(!isset($user_terms_and_conditions[$uid])){
      if(!isset($user_terms_and_conditions)) $user_terms_and_conditions = array();
      $user_terms_and_conditions[$uid] = new TACUser($uid);
    }
    return $user_terms_and_conditions[$uid];
  }


  public function __construct($uid){
    $this->uid = $uid;
    $this->tids = self::loadAcceptedTerms($uid);
  }

  /**
   * Load by the user accepted version of terms and conditions
   * Enter description here ...
   * @param unknown_type $uid
   *
   * @return assoctive array with tcid => user_table_object
   */
  public static function loadAcceptedTerms($uid){
    $q = db_select(TACVoc::TABLE_USER,"n")->fields("n");
    $q = $q->condition("n.uid",$uid)->execute();
    $results = $q->fetchAllAssoc(TACVoc::ID);
    $tids = array();
    foreach($results as $tcid => $t_obj){
      $tids[$tcid] = $t_obj;
    }
    return $tids;
  }


  function addHistoryMessage($tcid, $vid, $message){
    self::_addHistoryMessage($this->uid, $tcid, $vid, $message);
  }





  public static function _addHistoryMessage($uid, $tcid, $vid, $message){
    $args = func_get_args();

    if(count($args) > 4){
      $msg_args = array();
      for($i=4;$i<count($args);$i++){
        $msg_args[] = $args[$i];
      }
      $message = sprintf($message,$msg_args);
    }

    $data = array(
        "uid"=>$uid,
    TACVoc::ID=>$tcid,
    TACVoc::VID=>$vid,
        "message"=>$message,
        "timestamp"=>REQUEST_TIME);
    db_insert(TACVoc::TABLE_USER_HISTORY)->fields($data)->execute();
  }


  function save($user_accepted_terms, $accept = true){
    $transaction = db_transaction();
    try{
      foreach($user_accepted_terms as $t_version){
        $this->saveTerm($t_version,true);
      }
      $this->updateUserRoles();

    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception(TACVoc::ENTITY_TYPE, $e);
      throw $e;
    }
  }

  // TODO revoke user terms if they accepted
  // TODO revoke user roles if necassary
  function saveTerm(TACVersion $t_version, $accept = false){
    list($tcid, $vid) = TaCUtil::extractIds($t_version);

    $data = array(
    TACVoc::ID => $tcid,
    TACVoc::VID => $vid,
      'uid' => $this->uid,
      'timestamp' => REQUEST_TIME
    );

    $rid = TaCUtil::getFieldItemValue($t_version, TACVoc::F_ENABLE_ROLE);

    $role_name = TacUtil::checkRole($rid);
    if($role_name) $data['rid'] = $rid;

    // @todo only if accepted
    // only one revision of a node can be accepted, reset all entry of a nid
    if(!isset($this->tids[$tcid])){
      // insert
      db_insert(TACVoc::TABLE_USER)
      ->fields($data)
      ->execute();
      $this->addHistoryMessage($tcid, $vid, self::MSG_USER_ACCEPT_TERM);
    }else{
      // update
      $preview_vid = $this->tids[$tcid]->vid;
      $this->addHistoryMessage($tcid, $vid, self::MSG_USER_REVOKE_TERM);
      db_update(TACVoc::TABLE_USER)
      ->fields($data)
      ->condition("uid",$this->uid)
      ->condition(TACVoc::ID,$tcid)
      ->execute();
      $this->addHistoryMessage($tcid, $vid, self::MSG_USER_ACCEPT_TERM_CHANGE);
    }

    // register role for update
    if($role_name){
      if($accept){
        // add role
        $this->roles[$rid] = array("op"=>self::ROLE_ADD);
      }else{
        // remove role
        $this->roles[$rid] = array("op"=>self::ROLE_REMOVE);
      }
      $this->roles[$rid] += $data;
    }
  }

  function updateUserRoles(){
    $roles = user_roles(TRUE);
    $user = user_load($this->uid);

    $has_changed = false;
    foreach($this->roles as $rid => $data){
      if($data['op'] == self::ROLE_ADD){
        if(isset($user->roles[$rid])) continue;
        $user->roles[$rid] = $roles[$rid];
        $this->addHistoryMessage($data['tcid'], $data['vid'], self::MSG_ROLE_ADD,$roles[$rid]);
      }else if($data['op'] == self::ROLE_REMOVE){
        if(!isset($user->roles[$rid])) continue;
        unset($user->roles[$rid]);
        $this->addHistoryMessage($data['tcid'], $data['vid'], self::MSG_ROLE_REMOVE,$roles[$rid]);
      }else{
        continue;
      }
      $has_changed = true;

    }

    if($has_changed) user_save($user);
  }


  function isAccepted(TACVersion $t_version){
    list($tcid, $vid) = TaCUtil::extractIds($t_version);
    return (isset($this->tids[$tcid]) && $this->tids[$tcid]->vid == $vid) ? $this->tids[$tcid] : false;
  }

  /**
   * Returns the terms_and_conditions nodes for current mode and user role
   *
   * @param int $uid - User Id
   * @param array $term_ids
   * @param array $mode
   */
  function getVersions(array &$tc_ids, array $mode = array()){
    $user = user_load($this->uid);
    $tmp_t_versions = TACVersion::getVersions($tc_ids, $mode);

    $t_versions = array();
    foreach($tmp_t_versions as $hash => $t_version){
      // check if terms are only declared for an special role
      $rid = TACUtil::getActOnRole($t_version);
      $role_name = TacUtil::checkRole($rid);
      if($role_name){
        // verify the role for which the terms are specified
        if(!isset($user->roles[$rid])){
          unset($tc_ids[$hash]);
          continue;
        }
      }
      $t_versions[$hash] = $t_version;
    }

    return $t_versions;
  }




  function filterOutAccepted(array $tc_ids){
    $tmp_ids = $tc_ids;

    if(!empty($this->tids)){
      //  user has accepted some terms verify if they are actual
      foreach($tmp_ids as $hash => $t_obj){
        list($tcid, $vid) = TACUtil::extractIds($t_obj);
        if(isset($this->tids[$tcid])){

          if($this->tids[$tcid]->vid == $vid){
            // user has accepted the terms; remove accepted terms from list
            unset($tc_ids[$hash]);
          }else{
            // user has accepted the terms; but they changed ... new version is online
            $tc_ids[$hash]->user = "accept_new_revision";
          }
        }else{
          $tc_ids[$hash]->user = "accept_terms";
        }
      }
    }
    return $tc_ids;
  }

  /**
   * Callback for path user/%/terms_and_conditions
   *
   * @param  $uid
   */
  public static function overview($uid){
    // @todo user can see his own accepted terms, check this
    $tc_ids = TAC::getActive();
    $user_versions =& self::getInstance($uid);
    $t_versions = $user_versions->getVersions($tc_ids,
    array(TACVoc::MODE_ON_LOGIN,TACVoc::MODE_IN_USER_TERMS_OVERVIEW,TACVoc::MODE_ON_REGISTRATION));

    $header = array(t('Title'),t("Accepted"),t('Date'));
    $rows = array();

    foreach($t_versions as $hash => $t_version){
      $user_accept = $user_versions->isAccepted($t_version);

      $row = array();
      $title = TaCUtil::getTitle($t_version);
      $row[] = l($title,"user/$uid/".TACVoc::TYPE."/$hash");
      if($user_accept){
        $row[] = t('Accepted');
        $row[] = date("d.m.Y H:m:s",$user_accept->timestamp);
      }else{
        $row[] = t('No accepted');
        $row[] = "";
      }
      $rows[] = $row;
    }
    return theme("table",array("header"=>$header,"rows"=>$rows));

  }

  /**
   * Forms with awaiting acceptance terms_and_conditions node(s)
   */
  public static function form_accept_on_login(&$form,&$form_state){
    $t_versions = $form_state[TACVoc::FORM_STATE_NODES];

    if(empty($t_versions)){
      return;
    }
    unset($form['name'],$form['pass']);

    $form['name'] = array("#type"=>"hidden","#default_value" => $form_state['values']['name']);
    $form['pass'] = array("#type"=>"hidden","#default_value"=>"");

    $form[TACVoc::TYPE] = array("#tree"=>true);
    foreach($t_versions as $hash => $t_version){
      $form[TACVoc::TYPE][$hash] = theme("terms_and_conditions_accept_view",array("t_version"=>$t_version, "view_mode"=>"user-login"));
    }

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array('#type' => 'submit', '#value' => t('Forward'));
    return $form;
  }

  public static function form_accept_on_login_validate($form, &$form_state) {
    // form is embedded and validation redirect exists
    if(isset($form[TACVoc::TYPE])) return;

    // user is not authenticated, let the other validators work
    if(!isset($form_state['uid']) || !is_numeric($form_state['uid'])) return;

    $uid = $form_state['uid'];
    // user is not authenticated
    if($uid == 0) return;

    // user is superuser, doesn't need terms
    if($uid == 1) return;


    $user_versions =& TACUser::getInstance($form_state['uid']);
    $form_state[TACVoc::FORM_STATE_IDS] = $user_versions->filterOutAccepted($form_state[TACVoc::FORM_STATE_IDS]);

    // if outstanding terms are present
    if(!empty($form_state[TACVoc::FORM_STATE_IDS])){

      // load not accepted terms
      $form_state[TACVoc::FORM_STATE_NODES] = $user_versions->getVersions($form_state[TACVoc::FORM_STATE_IDS], array(TACVoc::MODE_ON_LOGIN));

      if(!empty($form_state[TACVoc::FORM_STATE_IDS])){
        // redirect the form to the terms accept form, only if there are some
        $form_state['rebuild_info'][TACVoc::TYPE] = true;
        $form_state['rebuild'] = true;
      }
    }
  }

  public static function form_accept_on_login_submit($form,&$form_state){
    self::form_submit($form, $form_state);
  }

  public static function isCurrentUser($uid){
    global $user;
    if(isset($user->uid) && isset($uid) && !empty($uid) && !empty($user->uid)){
      if(is_numeric($user->uid) && is_numeric($uid) && $user->uid === $uid) return true;
    }
    return false;
  }

  public static function form_accept_on_registration(&$form,&$form_state){
    $t_versions = $form_state[TACVoc::FORM_STATE_NODES];
    if(empty($t_versions)){
      return;
    }
    unset($form['account']['name'],$form['account']['mail']);

    $form['account']['name'] = array("#type"=>"hidden","#default_value" => $form_state['values']['name']);
    $form['account']['mail'] = array("#type"=>"hidden","#default_value"=>$form_state['values']['mail']);

    $form[TACVoc::TYPE] = array("#tree"=>true);
    foreach($t_versions as $hash => $t_version){
      $form[TACVoc::TYPE][$hash] = theme("terms_and_conditions_accept_view",array("t_version"=>$t_version, "view_mode"=>"user-login"));
    }
    return $form;
  }

  public static function form_accept_on_registration_validate($form,&$form_state){
    // form is embedded and validation redirect exists
    if(isset($form[TACVoc::TYPE])) return;

    // if outstanding terms are present
    if(!empty($form_state[TACVoc::FORM_STATE_IDS])){
      // redirect the form to the terms accept form, only if there are some
      $form_state['rebuild_info'][TACVoc::TYPE] = true;
      $form_state['rebuild'] = true;
    }

  }

  public static function form_accept_on_registration_submit($form,&$form_state){
    if(!isset($form_state['uid']) && isset($form_state['user']) && isset($form_state['user']->uid)){
      $form_state['uid'] = $form_state['user']->uid;
    }

    if(isset($form_state['uid'])){
      self::form_submit($form, $form_state);
    }else{
      // @todo what ???
    }
  }

  public static function form_accept_in_user_terms_overview($form,&$form_state, $uid = null, $hash = null){
    if(empty($hash) || !terms_and_conditions_has_user_access($uid)) drupal_access_denied();

    $form_state['uid'] = $uid;
    $tc_ids = TAC::getActive();

    // @todo pass argument to form
    if(!isset($tc_ids[$hash])){
      _terms_and_conditions_error_message("No such term and conditions found","user/$uid/".TACVoc::TYPE);
    }

    list($tcid, $vid) = TACUtil::extractIds($tc_ids[$hash]);

    $user_versions =& TACUser::getInstance($uid);

    $show_modes = array(TACVoc::MODE_ON_LOGIN,TACVoc::MODE_IN_USER_TERMS_OVERVIEW,TACVoc::MODE_ON_REGISTRATION);
    $form_state[TACVoc::FORM_STATE_NODES] = $user_versions->getVersions($tc_ids,$show_modes);
    $form_state[TACVoc::FORM_STATE_IDS] = $tc_ids;

    $user_data = $user_versions->isAccepted($form_state[TACVoc::FORM_STATE_NODES][$hash]);

    $form = array();
    $form[TACVoc::TYPE] = array("#tree"=>true);
    $form[TACVoc::TYPE][$hash] = theme("terms_and_conditions_accept_view",
    array("t_version"=>$form_state[TACVoc::FORM_STATE_NODES][$hash],"view_mode"=>"user-login",
      		  "user_data"=>$user_data));

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['back'] = array('#type' => 'submit', '#value' => t('Back'));
    $form['#submit'] = array();
    if(!$user_data){
      $form['actions']['submit'] = array(
          '#type' => 'submit',
          '#value' => t('Save'),
          '#description'=> t("This can't be redone."));
    }else{

    }
    list($class, $method) = explode("::",__METHOD__);
    $form_state[TAC_FORM_FLAG."_submit"] = array($class,$method."_submit");
    $form['#submit'][] = "terms_and_conditions_forms_wrapper_submit";

    return $form;
  }

  public static function form_accept_in_user_terms_overview_submit($form,&$form_state){
    $uid = $form_state['uid'];
    if(isset($form_state['values']['op'])){
      if($form_state['values']['op'] == t('Save')){
        $checked = TACUtil::filterFormCheckboxes($form_state['values'][TACVoc::TYPE],$form_state[TACVoc::FORM_STATE_IDS]);
        if(empty($checked)){
          drupal_set_message("Nothing changed");
        }else{
          self::form_submit($form, $form_state);
          drupal_set_message("Terms and conditions accepted");
        }
        return;
      }
    }
    drupal_goto("user/$uid/".TACVoc::TYPE);
  }


  public static function form_submit($form, &$form_state){
    $tc_ids = $form_state[TACVoc::FORM_STATE_IDS];

    // no terms are necassary
    if(empty($tc_ids)) return;

    $t_versions = $form_state[TACVoc::FORM_STATE_NODES];

    $checked = TaCUtil::filterFormCheckboxes($form_state['values'][TACVoc::TYPE], $tc_ids);
    $user_accepted_terms = array();

    foreach($checked as $hash => $dummy){
      if(!isset($t_versions[$hash])) continue;
      $user_accepted_terms[] = $t_versions[$hash];
    }

    $user_terms =& TaCUser::getInstance($form_state['uid']);
    $user_terms->save($user_accepted_terms, true);
  }


  public static function getStatistic($tcid = null, $vid = null){
    $where = "";
    if($tcid && $vid){
      $where = "WHERE tcid = $tcid AND vid = $vid";
    }
    $q = db_query("SELECT tcid, vid, COUNT(uid) as users FROM ".TACVoc::TABLE_USER." $where GROUP BY tcid, vid");
    $result = $q->fetchAll();
    $data = array();
    foreach($result as $_i => $obj){
      $data[$obj->tcid][$obj->vid] = $obj->users;
    }
    if($tcid && $vid){
      return isset($data[$tcid][$vid]) ? $data[$tcid][$vid] : 0;
    }

    return $data;
  }


}