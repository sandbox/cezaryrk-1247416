<?php

/**
 * Abstract db_object for safe db interaction
 *
 * @author cezaryrk
 *
 */

abstract class TACObject {
  public $is_new = true;

  public $clear_cache = true;
  public $tcid;

  public function __construct(){

  }

  public function save(){
    $transaction = db_transaction();

   try {
      if($this->is_new){
        $this->insert();
      }else{
        $this->update();
      }

      unset($this->is_new);

      // Clear the static loading cache.
      if($this->clear_cache) entity_get_controller(TACVoc::ENTITY_TYPE)->resetCache(array($this->tcid));

      // Ignore slave server temporarily to give time for the
      // saved node to be propagated to the slave.
      db_ignore_slave();
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception(TACVoc::ENTITY_TYPE, $e);
      throw $e;
    }

  }

  abstract public function insert();

  abstract public function update();

}