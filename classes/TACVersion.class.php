<?php

class TACVersion extends TACObject {

  const BUTTON_DETACH_USERS = "Detach users";
  const BUTTON_DELETE_REVISION = "Delete revision";

  const FORM_STATE_T_VERISON = "t_version";

  const MSG_DETACH_USER = "Revoke by admin, username: %s";


  public $tcid;
  public $vid = null;
  //public $label = "";
  public $timestamp;
  public $uid;

  public $clone = false;

  public function __construct($tcid, $vid = null){
    $this->tcid = $tcid;
    if(is_null($vid)){
      $this->is_new = true;
    }else{
      $this->is_new = false;
      $this->vid = $vid;
    }
  }


  public static function load($tcid, $vid, $reset = FALSE){
    if(!isset($vid) || empty($vid)) return null;
    $conditions = (isset($vid) ? array(TACVoc::VID => $vid) : array());

    $entity = entity_load(TACVoc::ENTITY_TYPE,array($tcid),$conditions, $reset);
    $entity = $entity ? reset($entity) : FALSE;
    if(!$entity) return FALSE;

    $tac = new TACVersion($entity->{TACVoc::ID},$entity->{TACVoc::VID});
    foreach($entity as $key => $value){
      $tac->{$key} = $value;
    }
    return $tac;
  }



  public function insert(){
    $this->timestamp = REQUEST_TIME;
    // check if an revision exists
    $entity = TAC::load($this->tcid);
    field_attach_presave(TACVoc::ENTITY_TYPE, $this);
    drupal_write_record(TACVoc::TABLE_REVISION, $this);
    $this->updateRevision();

    if(isset($this->clone) && $this->clone){
      // if it is a clone then update revision
      field_attach_update(TACVoc::ENTITY_TYPE, $this);
    }else{
      // else check if an version id is set, then update otherwise it is a new entry
      if(isset($entity->vid) && $entity->vid > 0) {
        field_attach_update(TACVoc::ENTITY_TYPE, $this);
      }else{
        field_attach_insert(TACVoc::ENTITY_TYPE, $this);
      }
    }
  }

  public function update(){
    $this->timestamp = REQUEST_TIME;
    field_attach_presave(TACVoc::ENTITY_TYPE, $this);
    drupal_write_record(TACVoc::TABLE_REVISION, $this, TACVoc::VID);
    field_attach_update(TACVoc::ENTITY_TYPE, $this);
  }

  public function delete($ignore_vid_update = FALSE){
    list($tcid,$vid) = TACUtil::extractIds($this);
    field_attach_delete_revision(TACVoc::ENTITY_TYPE, $this);
    db_delete(TACVoc::TABLE_REVISION)
    ->condition(TACVoc::ID,$tcid)
    ->condition(TACVoc::VID,$vid)
    ->execute();

    if(!$ignore_vid_update){
      // update the vid of root table
      $new_vid = db_select(TACVoc::TABLE_REVISION,"n")
      ->fields("n",array(TACVoc::VID))
      ->condition(TACVoc::ID,$tcid)->orderBy(TACVoc::VID,"ASC")->range(0,1)->execute()->fetchField();
      db_update(TACVoc::TABLE)->fields(array(TACVoc::VID => $new_vid))->condition(TACVoc::ID,$tcid)->execute();
    }
  }

  public function detachUsers(){
    global $user;
    $attached_users = db_select(TACVoc::TABLE_USER,"u")
    ->fields("u",array("uid"))
    ->condition("u.tcid",$this->tcid)
    ->condition("u.vid",$this->vid)
    ->execute()->fetchAllKeyed(0,0);
    foreach($attached_users as $uid){
      TACUser::_addHistoryMessage($uid, $this->tcid, $this->vid, self::MSG_DETACH_USER, $user->name."($user->uid)");
    }
    db_delete(TACVoc::TABLE_USER)->condition("tcid",$this->tcid)->condition("vid",$this->vid)->execute();
    return count($attached_users);
  }


  public function updateRevision(){
    db_update(TACVoc::TABLE)
    ->fields(array(TACVoc::VID => $this->{TACVoc::VID},"changed" => REQUEST_TIME))
    ->condition(TACVoc::ID, $this->{TACVoc::ID})
    ->execute();
  }


  static function getVersions(array &$tc_ids, array $mode = array()){
    // load entry terms
    $t_versions = array();
    $tmp_ids = $tc_ids;
    foreach($tmp_ids as $hash => $data){
      list($tcid, $vid) = TaCUtil::extractIds($data);
      $t_version = TACVersion::load($tcid, $vid);

      // verify the term mode
      $tc_modes = TaCUtil::getEnabledModes($t_version);
      $intersect = array_intersect($tc_modes, $mode);
      if(!empty($intersect)){
        $t_versions[$hash] = $t_version;
      }else{
        // remove entry for tc_ids
        unset($tc_ids[$hash]);
      }
    }
    return $t_versions;
  }


  /**
   * View for admins
   *
   *
   * @param unknown_type $tcid
   * @param unknown_type $vid
   * @return multitype:TACVersion unknown string
   */
  public static function view_admin($tcid, $vid){
    $t_version = self::load($tcid, $vid);
    return self::view($t_version, "full-admin");
  }


  public static function view(TACVersion $entity, $view_mode = 'full', $langcode = null){
    if (!isset($langcode)) {
      $langcode = $GLOBALS['language_content']->language;
    }

    // Remove previously built content, if exists.
    $entity->content = array();

    // Build fields content.
    field_attach_prepare_view(TACVoc::ENTITY_TYPE, array($entity->{TACVoc::ID} => $entity), $view_mode, $langcode);
    entity_prepare_view(TACVoc::ENTITY_TYPE, array($entity->{TACVoc::ID} => $entity), $langcode);

    $entity->content += field_attach_view(TACVoc::ENTITY_TYPE, $entity, $view_mode, $langcode);

    // Allow modules to make their own additions to the terms and conditions.
    module_invoke_all('entity_view', $entity, TACVoc::ENTITY_TYPE, $view_mode, $langcode);

    $build = $entity->content;
    // We don't need duplicate rendering info in entity->content.
    unset($entity->content);

    $build += array(
        '#theme' => TACVoc::TYPE,
        '#tac' => $entity,
        '#view_mode' => $view_mode,
        '#language' => $langcode,
    );
    return $build;
  }

  static function form($form, &$form_state, $tcid = null, $vid = null, $op = null) {
    if(is_numeric($tcid)){
      if(is_numeric($vid)){
        // load existing
        $terms_and_condtions = self::load($tcid, $vid);

        if ($op == 'clone') {
          // create a clone of the current version
          $terms_and_condtions->clone = true;
          $terms_and_condtions->is_new = true;
          $terms_and_condtions->vid = null;
        }
      }else{
        // create new
        $terms_and_condtions = new TACVersion($tcid);
      }
    }else if(is_object($tcid) && $tcid instanceof TACVersion){
      $terms_and_condtions = $tcid;
    }else{
      // TODO throw exception
      drupal_set_message("No id of terms and conditions is set");
      return array();
    }

    if($op == "edit"){
      $count = TACUser::getStatistic($tcid, $vid);
      if($count > 0){
        drupal_set_message("Can't modify terms and condition during users are attached. Detach users at first.","error");
        drupal_goto(TACVoc::ADMIN_PATH);
      }
    }

    $form_state[TACVoc::TYPE] = $terms_and_condtions;
    field_attach_form(TACVoc::ENTITY_TYPE, $terms_and_condtions, $form, $form_state);

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['submit'] = array(
        '#type' => 'submit',
        '#value' => t('Save'),
        '#access' => true,
        '#weight' => 19,
    );
    // @todo relize preview
    /*
    $form['actions']['preview'] = array(
    '#type' => 'submit',
    '#value' => t('Preview'),
    '#access' => (variable_get('comment_preview_' . $node->type, DRUPAL_OPTIONAL) != DRUPAL_DISABLED),
    '#weight' => 20,
    '#submit' => array('comment_form_build_preview'),
    );*/
    return $form;
  }

  public static function form_submit($form, &$form_state){
    $tac = $form_state[TACVoc::TYPE];
    entity_form_submit_build_entity(TACVoc::ENTITY_TYPE,$tac,$form,$form_state);
    $tac->save();
    $form_state['redirect'] = TACVoc::ADMIN_PATH;
  }


  public static function form_detach_users_confirm($form, &$form_state, $tcid = null, $vid = null){
    $t_version = null;
    if(is_numeric($tcid) && is_numeric($vid)){
      $t_version = self::load($tcid, $vid);
    }

    if(is_null($t_version)){
      // TODO throw exception
      drupal_set_message("Can't detach the users. Wrong parameter.","error");
      drupal_goto(TACVoc::ADMIN_PATH);
    }

    $form_state[self::FORM_STATE_T_VERISON] = $t_version;
    $count = TACUser::getStatistic($tcid,$vid);

    $form[TACVoc::TYPE] = array(
      '#markup' => t("Detach all @count users from this Revision?",array("@count"=>$count))
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['detach'] = array(
      '#type' => 'submit',
      '#value' => t(self::BUTTON_DETACH_USERS),
      '#access' => true,
      '#weight' => 19,
    );

    $form['actions']['no_detach'] = array(
      '#type' => 'submit',
      '#value' => t('Back'),
      '#access' => true,
      '#weight' => 20,
    );

    return $form;
  }

  public static function form_detach_users_confirm_submit($form, &$form_state){
    if($form_state['values']['op'] == t(self::BUTTON_DETACH_USERS)){
      $count = $form_state[self::FORM_STATE_T_VERISON]->detachUsers();
      drupal_set_message("$count users detached.");
    }
    drupal_goto(TACVoc::ADMIN_PATH);
  }

  public static function form_delete_confirm($form, &$form_state, $tcid = null, $vid = null){


    $t_version = null;
    if(is_numeric($tcid) && is_numeric($vid)){
      $t_version = self::load($tcid, $vid);
    }

    if(is_null($t_version)){
      // TODO throw exception
      drupal_set_message("Can't delete the revision. Wrong parameter.","error");
      drupal_goto(TACVoc::ADMIN_PATH);
    }

    $t_active = TAC::getActive();
    $hash = TACUtil::hash($tcid,$vid);
    if(isset($t_active[$hash])){
      drupal_set_message("Can't delete the revision, because it is active.","error");
      drupal_goto(TACVoc::ADMIN_PATH);
    }


    $count = TACUser::getStatistic($tcid,$vid);
    if($count !== 0){
      drupal_set_message("Can't delete the revision, because users are attached.","error");
      drupal_goto(TACVoc::ADMIN_PATH);
    }

    $form_state[self::FORM_STATE_T_VERISON] = $t_version;

    $form[TACVoc::TYPE] = array(
      '#markup' => t("Delete this revision permanently?",array("@count"=>$count))
    );

    $form['actions'] = array('#type' => 'actions');
    $form['actions']['delete'] = array(
      '#type' => 'submit',
      '#value' => t(self::BUTTON_DELETE_REVISION),
      '#access' => true,
      '#weight' => 19,
    );

    $form['actions']['no_delete'] = array(
      '#type' => 'submit',
      '#value' => t('Back'),
      '#access' => true,
      '#weight' => 20,
    );

    return $form;
  }

  public static function form_delete_confirm_submit($form, &$form_state){
    if($form_state['values']['op'] == t(self::BUTTON_DELETE_REVISION)){
      $count = $form_state[self::FORM_STATE_T_VERISON]->delete();
      drupal_set_message("Revision permanently deleted.");
    }
    drupal_goto(TACVoc::ADMIN_PATH);
  }


  public static function getAllRevisions($tcid) {
    $revisions = array();
    // TODO use db api
    $result = db_query("
      	SELECT r.vid, r.uid, r.timestamp, n.active_vid AS active_vid
      	FROM ".TACVoc::TABLE_REVISION." r
      	LEFT JOIN ".TACVoc::TABLE." n ON n.vid = r.vid"
    //INNER JOIN {users} u ON u.uid = r.uid
    ." WHERE r.tcid = :tcid ORDER BY r.vid DESC",
    array(':tcid' => $tcid));
    foreach ($result as $revision) {
      $revisions[$revision->{TACVoc::VID}] = $revision;
    }
    return $revisions;
  }


}