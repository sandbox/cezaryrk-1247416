<?php

/**
 * Terms and conditions vocabular
 *
 * @author cezaryrk
 *
 */
class TACVoc {
  
  const TYPE = "terms_and_conditions";
  
  const ENTITY_TYPE = "terms_and_conditions";
  const ENTITY_NAME = "terms_and_conditions_entity";
  
  // field names
  const F_TITLE = "terms_title";
  const F_BODY = "terms_body";
  const F_CHECKBOXES = "terms_accept_checkboxes";
  const F_REQUIRED = "terms_accept_required";
  const F_MODE = "terms_accept_mode";  
  const F_ON_ROLE = "terms_on_role";
  const F_ENABLE_ROLE = "terms_enable_role";
  
  
  // table ids
  const ID = "tcid";
  const UID = "tcuid";
  const VID = "vid";

  // table names  
  const TABLE = "terms_and_conditions";
  const TABLE_REVISION = "terms_and_conditions_revisions";
  const TABLE_USER = "terms_and_conditions_user";
  const TABLE_USER_HISTORY = "terms_and_conditions_user_history";
  
  // accept required
  const ACCEPT_OPTIONAL = "optional";
  const ACCEPT_REQUIRED = "required";
  
  // embed modi
  const MODE_ON_LOGIN = "accept_on_login";
  const MODE_ON_REGISTRATION = "accept_on_registration";
  const MODE_IN_USER_TERMS_OVERVIEW = "accept_in_user_terms_overview";

  // form_state names
  const FORM_STATE_IDS = "tac_ids";
  const FORM_STATE_NODES = "tac_nodes";
  //const FORM_STATE_HASHES = "hashmap";
  
  // paths
  const ADMIN_PATH = "admin/config/people/terms_and_conditions";
  
  
  
}