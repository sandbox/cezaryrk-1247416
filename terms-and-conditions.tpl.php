<div class="<?php print $classes; ?> clearfix">

  <h2><?php print $title; ?></h2>

  <div class="content" 
       style="margin:1em;overflow:auto;width:95%;min-height:60px; max-height:300px;border:1px solid #ccc;">   
    <?php print $body; ?>
  </div>


</div>
